from selenium import webdriver
import pandas as pd
import datetime as dt
import time

st = '01月13日'
et = '01月14日'
url = 'https://www.8btc.com/flash'
browser = webdriver.Chrome()
browser.get(url)


def set_time_range():
    date_inputs = browser.find_elements_by_tag_name('input')
    start_time = date_inputs[1]
    end_time = date_inputs[2]
    start_time.send_keys(st)
    end_time.send_keys(et)


def expand_page():
    page_end = False
    while not page_end:
        btn = browser.find_elements_by_class_name('bbt-btn')
        try:
            expand = btn[3]
            expand.click()
            time.sleep(3)
            print('Expanding...Please wait')
        except:
            print("Page expand end, Start getting data now..")
            page_end = True


def get_news():
    contents = browser.find_elements_by_class_name('flash-list')
    contents = contents[0].find_elements_by_class_name('list-item--wrap')
    all_news = []
    for news_content in contents:
        p_tag = news_content.find_elements_by_tag_name('p')
        title = p_tag[0].text
        source = p_tag[1].text.split('：')[1]
        text = p_tag[2].text
        span_tag = news_content.find_elements_by_tag_name('span')
        post_time = '2021-' + span_tag[0].text
        positive = span_tag[2].text.split(' ')[1]
        negative = span_tag[3].text.split(' ')[1]
        news = (title, source, post_time, text, positive, negative)
        all_news.append(news)
    return pd.DataFrame(all_news)


if __name__ == '__main__':
    set_time_range()
    expand_page()
    df = get_news()
    df.columns = ['title', 'source', 'datetime', 'content', 'pos', 'neg']
    file_name = str(dt.datetime.now()) + '.csv'
    print(file_name)
    df.to_csv(file_name, encoding='UTF-8')
    #print(df)
    print('All Done!')

